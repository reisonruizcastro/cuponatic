1. Descripción: 
---------------
el ejercicio fue realizado en "Laravel", por lo que se debe configurar el equipo antes de realizar las pruebas.


2. En el archivo ".env" se debe cambiar el nombre de base de datos, usuario y contraseña:
-----------------------------------------------------------------------------------------
DB_DATABASE=cuponatic
DB_USERNAME=root
DB_PASSWORD=123


3. DUMP de Base de Datos:
-------------------------
cuponatic.sql


4. Para poner a ejecutar el servicio "Laravel":
------------------------------------------------------------------------------------------------------------------------
php artisan serve


5. servicio que retorna los productos por keyword:
--------------------------------------------------
http://localhost:8000/api/search/{keyword}


6. servicio que retorna las estadisticas:
-----------------------------------------
http://localhost:8000/api/estadisticas


7. html para busqueda de productos:
-----------------------------------
http://localhost:8000/busqueda.html


8. html para estadisticas:
--------------------------
http://localhost:8000/estadistica.html

