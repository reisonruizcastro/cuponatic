<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Datos extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table      = 'datos_descuentos_buscador_prueba';
    protected $primaryKey = 'id';
    protected $fillable   = array('titulo');

}
