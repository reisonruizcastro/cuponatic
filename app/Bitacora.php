<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bitacora extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table      = 'bitacora_busqueda';
    protected $fillable   = array('titulo','palabra');

}
