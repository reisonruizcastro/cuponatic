<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Datos;
use App\Bitacora;
use DB;

class DatosController extends Controller
{

    // retorna la busqueda por la palabra introducida, solo los primero 50 registros 
    public function show($cadena)
    {        
        $registros = Datos::where('titulo', 'like', '%'.$cadena.'%')->limit(50)->get();        

        // se inserta en la bitacora para fines estadisticos
        foreach ($registros as $registro) {
            $bitacora = new Bitacora;
            $bitacora->titulo = $registro->titulo;
            $bitacora->palabra = $cadena;    
            $bitacora->save();
        }        
                
        return $registros;
    }

    // retorna json con el resumen estadistico
    public function estadisticas()
    {        

        $detalle_estadistica = array();

        $titulos = DB::table('bitacora_busqueda')
        ->select(DB::raw('count(titulo) as conteo, titulo'))
        ->groupBy('titulo')
        ->orderBy(DB::raw('conteo'),'desc')
        ->limit(20)
        ->get();

        $i = 0;
        foreach ($titulos as $titulo) {

            $detalle_estadistica[$i]['titulo'] = $titulo->titulo;
            
            $palabras = DB::table('bitacora_busqueda')
            ->select(DB::raw('count(palabra) as conteo, palabra'))
            ->where('titulo', '=', $titulo->titulo)
            ->groupBy('palabra')
            ->orderBy(DB::raw('conteo'),'desc')
            ->limit(5)
            ->get();            

            $listado_de_palabras = '';
            foreach ($palabras as $palabra) {
                $listado_de_palabras = $palabra->palabra.', '.$listado_de_palabras;
            }

            $detalle_estadistica[$i]['palabra'] = $listado_de_palabras;
            $i++;

        }

        return $detalle_estadistica;
    }

}
